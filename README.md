# Sustainyfacts Models

This projects contains the primary models used by Sustainyfacts:
- Data models for disclosure standards: GHG Protocol, SBTi, ESRS, SASB, GRI ...
- Rating models: SF Climate, ...

## License
This project is licensed under the GNU Affero General Public License. See [LICENSE](https://gitlab.com/sustainyfacts/models/-/blob/main/LICENSE).

## Standards Data Models
The folder data contains the data models for the standards supported by Sustainyfacts for disclosures. Each standard is described in a YAML file.

### Documentation

Disclosures and properties support the following attributes:

| Field | Mandatory | Constraints | Usage 
| ----- | --------- | ----------- | -----
| `id` | Yes | - | The well known, public ID of the metric/data point, if any. For example, GRI-305-1-a which is the "direct (Scope 1) GHG emissions" from the GRI standard. 
| `internal_id` | Yes | Alphanumeric with "_" | Internal sustainyfact ID, used for reference and calculations. This `internal_id` might be shared by two metrics of different standards, **if** they have the same definition.
| `name` | Yes | - | Name of the metric/data point
| `description` | No | - | Description of the metric. Use the standard's own description/definition of the metric with as much details as possible.
| `type` | Yes | `integer`, `decimal`, `string`, `object`, or `array of [type]` | Type of data. The simple types are interger, decimal and string. The complex types are arrays and objects. Arrays can contain any type: `array of _type_`. Objects must be described with a list of properties.
| `unit` | No | - | For type integers or decimals, specify which unit must be used
| `properties` | No | list of properties | Contains the list of properties for a type `object` or `array of object`.

### Example

Below is an example file.

```
name: Sustainyfacts Reporting Standard
id: SFRS
description: More detailled description of the standard
version: "0.1"
disclosures:
  - id: SF-TST-001
    internal_id: sf_tst_001
    name: Scope 1 Emissions
    description: Direct greenhouse gas emissions from owned or controlled sources
    type: decimal
    unit: metric_tons_CO2e
  - id: SF-TST-002
    internal_id: sf_tst_002
    name: Integer Array
    description: An array of integers
    type: array of integer
    unit: FTE
  - id: SF-TST-003
    internal_id: sf_tst_003
    name: Decimal Array
    description: An array of decimals
    type: array of decimal
    unit: Wh
  - id: SF-TST-004
    internal_id: sf_tst_004
    name: String Array
    description: An array of strings
    type: array of string
  - id: SF-TST-005
    internal_id: sf_tst_005
    name: Object
    description: An object with 2 properties
    type: object
    properties:
      - id: SF-TST-005a
        internal_id: metric1
        name: Something
        description: Bla bla bla
        type: decimal
        unit: MWh
      - id: SF-TST-005b
        internal_id: metric2
        name: Percentage of something
        description: Bla bla bla 2
        type: integer
        unit: "%"
  - id: SF-TST-006
    internal_id: sf_tst_006
    name: Emissions Reduction Targets
    description: Targets for reducing greenhouse gas emissions 
    type: array of object
    properties:
      - id: SF-TST-006a
        internal_id: target_year
        name: Target Year
        description: Year for the target
        type: integer
      - id: SF-TST-006b
        internal_id: target_value
        name: Target Value
        description: Scope 1 Emmissions target
        type: decimal
        unit: metric_tons_CO2e
```
